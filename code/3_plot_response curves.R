load(file = "./data/env_predict.RData")

sp_select <- "Greenstriped Rockfish"

colV <- c("dodgerblue2", "red", "#FDE725FF")


survey_env <- all_sets_wide %>% 
  st_drop_geometry() %>% 
  select(Survey = Survey_base, depth = depth_m, salinity, salinity.range, muddy, rocky, BBPI, circulation, tidal) %>% 
  mutate(Survey = ifelse(Survey == "SOG_Syn", "SoG_Trawl", Survey))


depth_p <- depth.df %>% 
  filter(species == sp_select) %>% 
  ggplot(aes(x = depth, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+
  ylim(c(0,1))+
  ylab("Probabiliy of Occurrence")+
  xlab("Depth (m)")+
  xlim(c(30,1000))+
  geom_point(data = survey_env, aes(x = depth, y = 1, color = NA, fill = NA), pch = "|", color = "grey30")+
  theme(strip.background = element_blank(), strip.text.y = element_blank())


salt_p <- salinity.df %>% 
  filter(species == sp_select) %>% 
  ggplot(aes(x = salinity, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+
  ylim(c(0,1))+
  ylab("")+
  xlab("Salinity")+
  xlim(c(min(salinity.df$salinity),max(salinity.df$salinity)))+
  geom_point(data = survey_env, aes(y = 1, color = NA, fill = NA), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank(), strip.background = element_blank(), strip.text.y = element_blank())


salt2_p <- salinity_range.df %>% 
  filter(species == sp_select) %>% 
  ggplot(aes(x = salinity.range, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+
  ylim(c(0,1))+
  ylab("")+
  xlab("Salinity range")+
  xlim(c(min(salinity_range.df$salinity.range),max(salinity_range.df$salinity.range)))+
  geom_point(data = survey_env, aes(y = 1, color = NA, fill = NA), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank(), strip.background = element_blank(), strip.text.y = element_blank())


rock_p <- rocky.df %>% 
  filter(species == sp_select) %>% 
  ggplot(aes(x = rocky, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+
  ylim(c(0,1))+
  ylab("")+
  xlab("Rockiness")+
  xlim(c(min(rocky.df$rocky),max(rocky.df$rocky)))+
  geom_point(data = survey_env, aes(y = 1, color = NA, fill = NA), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank(), strip.background = element_blank(), strip.text.y = element_blank())


mud_p <- muddy.df %>% 
  filter(species == sp_select) %>% 
  ggplot(aes(x = muddy, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+
  ylim(c(0,1))+
  ylab("")+
  xlab("Muddiness")+
  xlim(c(min(muddy.df$muddy),max(muddy.df$muddy)))+
  geom_point(data = survey_env, aes(y = 1, color = NA, fill = NA), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank(), strip.background = element_blank(), strip.text.y = element_blank())


circ_p <- circ.df %>% 
  filter(species == sp_select) %>% 
  ggplot(aes(x = circulation, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+
  ylim(c(0,1))+
  ylab("")+
  xlab("Circulation")+
  xlim(c(min(circ.df$circulation),max(circ.df$circulation)))+
  geom_point(data = survey_env, aes(y = 1, color = NA, fill = NA), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank(), strip.background = element_blank(), strip.text.y = element_blank())


tidal_p <- tidal.df %>% 
  filter(species == sp_select) %>% 
  ggplot(aes(x = tidal, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "")+
  scale_fill_manual(values = colV, name = "", guide = "none")+
  ylim(c(0,1))+
  ylab("")+
  xlab("Tidal speed")+
  xlim(c(min(tidal.df$tidal),max(tidal.df$tidal)))+
  geom_point(data = survey_env, aes(y = 1, color = NA, fill = NA), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank())+
  theme(legend.position = "bottom", legend.justification = c(1))

BPI_p <- BBPI.df %>% 
  filter(species == sp_select) %>% 
  ggplot(aes(x = BBPI, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+
  ylim(c(0,1))+
  ylab("")+
  xlab("BBPI")+
  xlim(c(min(BBPI.df$BBPI),max(BBPI.df$BBPI)))+
  geom_point(data = survey_env, aes(y = 1, color = NA, fill = NA), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank(), strip.background = element_blank(), strip.text.y = element_blank())

depth_p | BPI_p | salt_p | salt2_p | rock_p | mud_p | circ_p | tidal_p
