library(sdmTMB)
library(DHARMa)
library(tidyverse)
library(sf)

load("./data/pre_analysis.RData")

all_sets_wide <- all_sets_wide %>% 
  mutate(Survey_base = Survey) %>% 
  mutate(Survey = ifelse(Survey == "SOG_Syn", "Syn_trawl", ifelse(Survey == "Outer_Syn", "Syn_trawl", Survey)))

speciesV <- colnames(all_sets_wide %>% st_set_geometry(NULL) %>% dplyr::select("Alaska Skate":"Yellowtail Rockfish"))[colSums(all_sets_wide %>% st_set_geometry(NULL) %>% dplyr::select("Alaska Skate":"Yellowtail Rockfish"))>150]
surveys <- c("All", "Syn_trawl" ,"Longline", as.character(unique(all_sets_wide$Survey_base)))

ran_params.df <- data.frame()
for(species in speciesV){
  print(species)
  load(paste("./data/models/", species,".RData", sep = ""))
  for(i in 1:length(surveys)){
    
    fit <- models[[i]]
    print(surveys[i])
    if(!is.na(fit)){
      ran_param <- tidy(fit, "ran_pars", conf.int = TRUE)
      ran_param$species <- species
      ran_param$model = surveys[i]
      
      ran_params.df <- rbind(ran_params.df, ran_param)
    }
  }
}

save(ran_params.df, file = "./data/ran_param.RData")

ran_params.df %>% 
  mutate(model = ifelse(model == "All", "Fully integrated", model)) %>% 
  mutate(model = factor(model, levels = rev(c("Fully integrated", surveys[-1])), ordered = TRUE)) %>% 
  arrange(model) %>% 
  ggplot(aes(x = species, y = estimate, color = model))+
  geom_vline(xintercept = speciesV[seq(1,length(speciesV),3)], linetype = 2)+
  geom_point()+
  facet_wrap(~term)+
  coord_flip()+
  xlab("")+
  scale_y_sqrt(breaks = c(5, 100, 500, 1000, 2000))+
  scale_color_manual(values = rev(c(1, "dodgerblue2", "grey", "forestgreen", "red", "gold", "mediumorchid1")))



ran_params.df %>% 
  mutate(model = ifelse(model == "All", "Fully integrated", model)) %>% 
  left_join(data.frame(model = c("Fully integrated", surveys[-1]), type = c("Fully integrated", "Integrated gear", "Integrated gear", "Single survey", "Single survey", "Single survey", "Single survey"))) %>% 
  mutate(model = factor(model, levels = rev(c("Fully integrated", surveys[-1])), ordered = TRUE)) %>% 
  arrange(model) %>% 
  ggplot(aes(x = estimate, y = model))+
  geom_vline(data = ran_params.df %>% 
               group_by(term) %>% 
               filter(model == "All") %>% 
               summarise(med = median(estimate)), aes(xintercept = med, model = NULL, x = NULL), linetype = 2)+
  geom_boxplot(aes(fill = type))+
  facet_wrap(~term)+
  scale_x_sqrt(breaks = c(5, 100, 500, 1000, 2000))+
  scale_fill_manual(values = c("dodgerblue2", "red", "#FDE725FF"), name = "")


ran_params.df %>% 
  mutate(conf = conf.high - conf.low) %>% 
  filter(conf < 1e+07) %>% 
  mutate(model = ifelse(model == "All", "Fully integrated", model)) %>% 
  left_join(data.frame(model = c("Fully integrated", surveys[-1]), type = c("Fully integrated", "Integrated gear", "Integrated gear", "Single survey", "Single survey", "Single survey", "Single survey"))) %>% 
  mutate(model = factor(model, levels = rev(c("Fully integrated", surveys[-1])), ordered = TRUE)) %>% 
  arrange(model) %>% 
  ggplot(aes(x = conf, y = model))+
  geom_vline(data = ran_params.df %>% 
               mutate(conf = conf.high - conf.low) %>% 
               group_by(term) %>% 
               filter(model == "All") %>% 
               summarise(med = median(conf)), aes(xintercept = med, model = NULL, x = NULL), linetype = 2)+
  geom_boxplot(aes(fill = type))+
  facet_wrap(~term)+
  scale_x_log10()+
  scale_fill_manual(values = c("dodgerblue2", "red", "#FDE725FF"), name = "")
  