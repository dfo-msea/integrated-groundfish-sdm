# Code to produce analysis for - Integrating trawl and longline surveys across British Columbia improves groundfish distribution predictions

__Main author:__  Patrick Thompson  
__Contributors:__ Sean Anderson, Jessica Nephin, Beatrix Proudfoot, Ashley Park, Dana Haggarty, Emily Rubidge  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: patrick.thompson@dfo-mpo.gc.ca


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Coastwide model that integrates longline and trawl surveys to predict groundfish species distributions. This will be used to produce spatial layers of estimated species occurrence for Marine Spatial Planning.
## Summary

## Status
Complete

## Contents
R code for loading data and performing analysis. 

## Methods
Using sdmTMB package to integrate the Synoptic Trawl (including Strait of Georgia), Hard Bottom Long Line, and IPHC Surveys.

## Requirements

## Caveats

