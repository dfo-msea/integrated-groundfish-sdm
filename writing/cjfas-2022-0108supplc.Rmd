---
title: "Integrating trawl and longline surveys across British Columbia improves groundfish distribution predictions"
subtitle: "Supplementary materials C: single species environmental response model comparisons."
author: 
  - Patrick L. Thompson, Sean C. Anderson, Jessica Nephin,  
  - Carolyn K. Robb, Beatrice Proudfoot, Ashley E. Park, Dana Haggarty, Emily Rubidge
output:
    bookdown::pdf_document2:
      toc: false
      number_sections: false
      fig_caption: false
      keep_tex: false
csl: CJFAS.csl
bibliography: Integrated_GF_model.bib
always_allow_html: true
link-citations: yes
header-includes:
  \usepackage{gensymb}
  \newcommand{\beginsupplement}{
  \setcounter{equation}{0}
  \renewcommand{\theequation}{S.\arabic{equation}}
  \setcounter{table}{0}  
  \renewcommand{\thetable}{S\arabic{table}} 
  \setcounter{figure}{0} 
  \renewcommand{\thefigure}{S\arabic{figure}}}
  \usepackage{fancyhdr}
  \pagestyle{fancy}
  \fancyhf{}
  \fancyfoot[C]{\thepage}
  \rhead{Integrated Groundfish SDM - Supplementary Materials C}
  \usepackage{xr}
  \usepackage{pdflscape}
  \newcommand{\blandscape}{\begin{landscape}}
  \newcommand{\elandscape}{\end{landscape}}
  \extrafloats{100}
---

```{r, include=FALSE}
options(tinytex.verbose = TRUE)
```

```{r, echo=FALSE, include = FALSE}
# devtools::install_github("cboettig/knitcitations@v1")
library(knitcitations); cleanbib()
cite_options(citation_format = "pandoc", check.entries=FALSE)
library(bibtex)
library(knitr)
library(kableExtra)
```

```{r load_packages, include=FALSE}
#data manipulation
library(tidyverse)

#plotting
library(corrplot)
library(viridis)
library(RColorBrewer)
#library(cowplot)
library(sf)
library(PupillometryR)
library(scales)
library(patchwork)
```

```{r include = FALSE}
load("../data/sdmTMB_integrated_maps.RData")
load("../data/integrated_gf_maps_selected.RData")
load("../data/pre_analysis.RData")
load("../data/env_predict.RData")
summary_models <- read.csv("../data/selected_model_summary.csv")
metrics <- read.csv("../data/metrics.csv")

source("../code/functions/plt_plot_theme.R")
gf_names.df <- read.csv("../data/gf_names.csv")

theme_set(plt_theme)

knitr::opts_chunk$set(dev="png")

all_sets_wide <- all_sets_wide %>% 
  mutate(Survey = ifelse(Survey == "Outer_Syn", "Outer Trawl", ifelse(Survey == "Hardbottom_LL", "HBLL", ifelse(Survey == "SOG_Syn", "SoG Trawl", Survey))))

all_sets_wide$`Blackfin Sculpin` <- NULL #removing Blackfin Sculpin because it is misslabelled in the database

selected_maps <- selected_maps %>% filter(species != "Blackfin Sculpin")

metrics <- metrics %>% 
  filter(species != "Blackfin Sculpin") %>% 
  mutate(Survey = ifelse( Survey == "Outer_Syn", "Outer Trawl", ifelse(Survey == "SOG_Syn", "SoG Trawl", Survey))) %>% 
  mutate(type2 = ifelse(type2 == "Full Integrated", "Fully Integrated", type2))


survey_env <- all_sets_wide %>% 
  st_drop_geometry() %>% 
  select(Survey = Survey, depth = depth_m, salinity, salinity.range, muddy, rocky, BBPI, circulation, tidal)
```

\newpage
\blandscape
```{r specPlots, fig.align="center", fig.height = 8*1.3, fig.width = 8*1.5, out.width = "95%", echo=FALSE, fig.retina = 3, warning = FALSE, results='hide', message=FALSE}
for(i in unique(selected_maps$species)[order(unique(selected_maps$species))]){
  
  sci_name <- gf_names.df$science_name[gf_names.df$species_common_name == i]
  
  m_summary <- summary_models %>% filter(species == i)
  
  colV <- rev(c("dodgerblue2", "red", "#FDE725FF"))

survey_env <- all_sets_wide %>% 
  st_drop_geometry() %>% 
  select(Survey = Survey, depth = depth_m, salinity, salinity.range, muddy, rocky, BBPI, circulation, tidal)


depth_p <- depth.df %>% 
  mutate(Survey = ifelse(Survey == "Outer_Syn", "Outer Trawl", ifelse(Survey == "Hardbottom_LL", "HBLL", ifelse(Survey == "SoG_Trawl", "SoG Trawl", Survey)))) %>% 
  mutate(type2 = str_to_title(type2)) %>% 
  mutate(type2 = ifelse(type2 == "Full Integrated", "Fully Integrated", type2)) %>% 
  mutate(type2 = factor(type2, levels = c("Single", "Integrated Gear", "Fully Integrated"), ordered = TRUE)) %>% 
  filter(species == i) %>% 
    ggplot(aes(x = depth, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+

  ylab("Probability of Occurrence")+
  xlab("Depth (m)")+
  xlim(c(0,1000))+
  geom_point(data = survey_env, aes(x = depth, y = 1, color = NULL, fill = NULL), pch = "|", color = "grey30")+
  theme(strip.background = element_blank(), strip.text.y = element_blank())


salt_p <- salinity.df %>% 
   mutate(Survey = ifelse(Survey == "Outer_Syn", "Outer Trawl", ifelse(Survey == "Hardbottom_LL", "HBLL", ifelse(Survey == "SoG_Trawl", "SoG Trawl", Survey)))) %>% 
 mutate(type2 = str_to_title(type2)) %>% 
  mutate(type2 = ifelse(type2 == "Full Integrated", "Fully Integrated", type2)) %>% 
  mutate(type2 = factor(type2, levels = c("Single", "Integrated Gear", "Fully Integrated"), ordered = TRUE)) %>% filter(species == i) %>% 
  mutate(est = ifelse(Survey == "SoG Trawl" & type2 == "Single", NA, est)) %>%
  ggplot(aes(x = salinity, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+

  ylab("")+
  xlab("Salinity")+
  xlim(c(min(salinity.df$salinity),max(salinity.df$salinity)))+
  geom_point(data = survey_env, aes(y = 1, color = NULL, fill = NULL), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank(), strip.background = element_blank(), strip.text.y = element_blank())


salt2_p <- salinity_range.df %>% 
   mutate(Survey = ifelse(Survey == "Outer_Syn", "Outer Trawl", ifelse(Survey == "Hardbottom_LL", "HBLL", ifelse(Survey == "SoG_Trawl", "SoG Trawl", Survey)))) %>% 
mutate(type2 = str_to_title(type2)) %>% 
  mutate(type2 = ifelse(type2 == "Full Integrated", "Fully Integrated", type2)) %>% 
  mutate(type2 = factor(type2, levels = c("Single", "Integrated Gear", "Fully Integrated"), ordered = TRUE)) %>% filter(species == i) %>% 
  mutate(est = ifelse(Survey == "SoG Trawl" & type2 == "Single", NA, est)) %>%
  ggplot(aes(x = salinity.range, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+

  ylab("")+
  xlab("Salinity range")+
  xlim(c(min(salinity_range.df$salinity.range),max(salinity_range.df$salinity.range)))+
  geom_point(data = survey_env, aes(y = 1, color = NULL, fill = NULL), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank(), strip.background = element_blank(), strip.text.y = element_blank())


rock_p <- rocky.df %>% 
   mutate(Survey = ifelse(Survey == "Outer_Syn", "Outer Trawl", ifelse(Survey == "Hardbottom_LL", "HBLL", ifelse(Survey == "SoG_Trawl", "SoG Trawl", Survey)))) %>% 
  filter(species == i) %>% 
  mutate(est = ifelse(Survey == "SoG Trawl" & type2 == "Single", NA, est)) %>%
 mutate(type2 = str_to_title(type2)) %>% 
  mutate(type2 = ifelse(type2 == "Full Integrated", "Fully Integrated", type2)) %>% 
  mutate(type2 = factor(type2, levels = c("Single", "Integrated Gear", "Fully Integrated"), ordered = TRUE)) %>% ggplot(aes(x = rocky, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+

  ylab("")+
  xlab("Rockiness")+
  xlim(c(min(rocky.df$rocky),max(rocky.df$rocky)))+
  geom_point(data = survey_env, aes(y = 1, color = NULL, fill = NULL), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank(), strip.background = element_blank(), strip.text.y = element_blank())


mud_p <- muddy.df %>% 
   mutate(Survey = ifelse(Survey == "Outer_Syn", "Outer Trawl", ifelse(Survey == "Hardbottom_LL", "HBLL", ifelse(Survey == "SoG_Trawl", "SoG Trawl", Survey)))) %>% 
  filter(species == i) %>% 
  mutate(est = ifelse(Survey == "SoG Trawl" & type2 == "Single", NA, est)) %>%
mutate(type2 = str_to_title(type2)) %>% 
  mutate(type2 = ifelse(type2 == "Full Integrated", "Fully Integrated", type2)) %>% 
  mutate(type2 = factor(type2, levels = c("Single", "Integrated Gear", "Fully Integrated"), ordered = TRUE)) %>% ggplot(aes(x = muddy, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+

  ylab("")+
  xlab("Muddiness")+
  xlim(c(min(muddy.df$muddy),max(muddy.df$muddy)))+
  geom_point(data = survey_env, aes(y = 1, color = NULL, fill = NULL), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank(), strip.background = element_blank(), strip.text.y = element_blank())


circ_p <- circ.df %>% 
   mutate(Survey = ifelse(Survey == "Outer_Syn", "Outer Trawl", ifelse(Survey == "Hardbottom_LL", "HBLL", ifelse(Survey == "SoG_Trawl", "SoG Trawl", Survey)))) %>% 
  filter(species == i) %>% 
  mutate(est = ifelse(Survey == "SoG Trawl" & type2 == "Single", NA, est)) %>%
 mutate(type2 = str_to_title(type2)) %>% 
  mutate(type2 = ifelse(type2 == "Full Integrated", "Fully Integrated", type2)) %>% 
  mutate(type2 = factor(type2, levels = c("Single", "Integrated Gear", "Fully Integrated"), ordered = TRUE)) %>% ggplot(aes(x = circulation, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+

  ylab("")+
  xlab("Circulation")+
  xlim(c(min(circ.df$circulation),max(circ.df$circulation)))+
  geom_point(data = survey_env, aes(y = 1, color = NULL, fill = NULL), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank(), strip.background = element_blank(), strip.text.y = element_blank())


tidal_p <- tidal.df %>% 
   mutate(Survey = ifelse(Survey == "Outer_Syn", "Outer Trawl", ifelse(Survey == "Hardbottom_LL", "HBLL", ifelse(Survey == "SoG_Trawl", "SoG Trawl", Survey)))) %>% 
  filter(species == i) %>% 
  mutate(est = ifelse(Survey == "SoG Trawl" & type2 == "Single", NA, est)) %>%
mutate(type2 = str_to_title(type2)) %>% 
  mutate(type2 = ifelse(type2 == "Full Integrated", "Fully Integrated", type2)) %>% 
  mutate(type2 = factor(type2, levels = c("Single", "Integrated Gear", "Fully Integrated"), ordered = TRUE)) %>% ggplot(aes(x = tidal, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "")+
  scale_fill_manual(values = colV, name = "", guide = "none")+

  ylab("")+
  xlab("Tidal speed")+
  xlim(c(min(tidal.df$tidal),max(tidal.df$tidal)))+
  geom_point(data = survey_env, aes(y = 1, color = NULL, fill = NULL), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank())+
  theme(legend.position = "bottom", legend.justification = c(1))


BPI_p <- BBPI.df %>% 
   mutate(Survey = ifelse(Survey == "Outer_Syn", "Outer Trawl", ifelse(Survey == "Hardbottom_LL", "HBLL", ifelse(Survey == "SoG_Trawl", "SoG Trawl", Survey)))) %>% 
  filter(species == i) %>% 
  mutate(est = ifelse(Survey == "SoG Trawl" & type2 == "Single", NA, est)) %>%
mutate(type2 = str_to_title(type2)) %>% 
  mutate(type2 = ifelse(type2 == "Full Integrated", "Fully Integrated", type2)) %>% 
  mutate(type2 = factor(type2, levels = c("Single", "Integrated Gear", "Fully Integrated"), ordered = TRUE)) %>% ggplot(aes(x = BBPI, y = plogis(est), color = type2, fill = type2))+
  geom_ribbon(aes(ymin = plogis(est - est_se*2), ymax = plogis(est + est_se*2)), color = NA, alpha = 0.35)+
  geom_line(size = 1)+
  facet_grid(Survey~.)+
  scale_color_manual(values = colV, name = "", guide = "none")+
  scale_fill_manual(values = colV, name = "", guide = "none")+

  ylab("")+
  xlab("BPI")+
  xlim(c(min(BBPI.df$BBPI),max(BBPI.df$BBPI)))+
  geom_point(data = survey_env, aes(y = 1, color = NULL, fill = NULL), pch = "|", color = "grey30")+
  theme(axis.title.y = element_blank(), strip.background = element_blank(), strip.text.y = element_blank())


caption <- paste("Estimated environmental response curves for ", i , " (", sci_name, ") as an example of how the integrated models estimate coefficents\nthat are consistent with other data sources. Estimates are with all other predictors set at their mean values. Only a select set of environmental covariates\nused in the model are shown. The models trained on just one survey are shown in yellow, the integrated gear models (trained on the HBLL and IPHC\ntogether, or the Outer Trawl and SoG Trawl together) are shown in red, the  fully integrated models that are trained on all four surveys are shown in blue.\nEach row shows the survey (fixed effect) used to make the predictions, labelled on the righthand side. The lines show the mean estimated values and the\nbands show the 95% confidence intervals, which include intercept and main effect uncertainty but exclude random field uncertainty. The ticks at the top of\nthe panels show the environmental conditions for all the individual survey sets. Note that only depth is included in the single model for the SoG Trawl. Our\ndecision rules recommended making predictions for ", i, " using the, ", m_summary$model, " model with fixed effect for survey set to ", m_summary$survey,".", sep = "")
  
env_curves <- depth_p | BPI_p | salt_p | rock_p | tidal_p
  
  final_plot <- env_curves + plot_annotation(tag_levels = 'a', title = i , subtitle = sci_name, caption = caption, theme = theme(plot.title = element_text(size = 24, hjust = 0), plot.subtitle = element_text(size = 20, hjust = 0, face = "italic"), plot.caption = element_text(size = 12, hjust = 0)))
  print(final_plot)
}

```
\elandscape